//! Parser infrastructure for a Repeatable

use crate::model::Repeatable;
use pest::{
	error::{Error, ErrorVariant},
	Parser,
};
use pest_derive::Parser;
use std::mem;

#[derive(Debug, Parser)]
#[grammar = "parser/repeatable.pest"]
struct RepeatableParser;

/// Parses a string into a repeatable.
/// The outer errors are errors encountered while parsing (formatting issues,
/// number conversion issues.)
/// The inner errors is when the user gave a valid string, but is missing
/// semantic options (missing required elements, making min > max and so on)
#[tracing::instrument]
pub fn parse(s: &str) -> Result<Result<Repeatable, String>, Error<Rule>> {
	let parsed = RepeatableParser::parse(Rule::parsed, s)?;

	// These are the fields we need to parse out
	let mut trigger: Option<String> = None;
	let mut response: Option<String> = None;
	let mut min: Option<u8> = None;
	let mut max: Option<u8> = None;

	// We made the outer rule silent, so we can just iterate over the pairs
	for item in parsed {
		tracing::trace!(item = item.as_str(), "parsing item");
		let span = item.as_span().clone();
		let mut inner = item.into_inner();

		// the "key" part of the kv string
		let opt = match inner.next() {
			Some(value) => value,
			None => {
				return Err(Error::new_from_span(
					ErrorVariant::CustomError {
						message: String::from("missing name for argument (opt)"),
					},
					span,
				))
			}
		};

		// the "value" part of the kv string
		let val = match inner.next() {
			Some(value) => value,
			None => {
				return Err(Error::new_from_span(
					ErrorVariant::CustomError {
						message: String::from("missing name for argument (val)"),
					},
					span,
				))
			}
		};

		// populate the options defined above
		match opt.as_str() {
			"trigger" | "t" => {
				let old = mem::replace(&mut trigger, Some(String::from(val.as_str())));
				tracing::trace!(old=?old, new=?trigger, "found trigger opt");
			}
			"response" | "r" => {
				let old = mem::replace(&mut response, Some(String::from(val.as_str())));
				tracing::trace!(old=?old, new=?response, "found response opt");
			}
			"min" | "l" => {
				// this should be (almost) the same as max so i could
				// probably shove this into a function?
				// TODO: See if a function works here
				let newv = match val.as_str().parse::<u8>() {
					Ok(val) => val,
					Err(_) => {
						return Err(Error::new_from_span(
							ErrorVariant::CustomError {
								// we are loosing the actual error information here, we might want
								// to include it somehow?
								message: String::from("unable to parse number"),
							},
							val.as_span(),
						));
					}
				};

				let old = mem::replace(&mut min, Some(newv));
				tracing::trace!(old=?old, new=?min, "found min opt");
			}
			"max" | "h" => {
				// TODO: See if a function works here (@see min)
				let newv = match val.as_str().parse::<u8>() {
					Ok(val) => val,
					Err(_) => {
						return Err(Error::new_from_span(
							ErrorVariant::CustomError {
								// we are loosing the actual error information here, we might want
								// to include it somehow?
								message: String::from("unable to parse number"),
							},
							val.as_span(),
						));
					}
				};

				let old = mem::replace(&mut max, Some(newv));
				tracing::trace!(old=?old, new=?max, "found min opt");
			}
			_ => {
				return Err(Error::new_from_span(
					ErrorVariant::CustomError { message: String::from("unknown option key") },
					opt.as_span(),
				))
			}
		}
	}

	// we cant work without a trigger.
	if trigger.is_none() {
		return Ok(Err(String::from("the trigger field is required!")));
	}

	// construct the object, sanity checks follow this
	let repeatable = Repeatable::new(
		trigger.clone().unwrap(),
		response.or_else(|| trigger).unwrap(),
		min.unwrap_or(1),
		max.unwrap_or(5),
	);

	if repeatable.min() == 0 {
		return Ok(Err(String::from("the minimum cannot be 0")));
	}

	// sanity check. we could make this a non-error my saying
	// well min > max, so lets swap them.
	// but we are keeping it as a error right now
	if repeatable.min() > repeatable.max() {
		return Ok(Err(String::from("the minimum cannot be larger than the maximum")));
	}

	Ok(Ok(repeatable))
}
