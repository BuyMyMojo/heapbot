use crate::model::{ExpandItem, Expandable};
use pest::{error::InputLocation as Location, Parser};
use pest_derive::Parser;

#[derive(Debug, Parser)]
#[grammar = "parser/custom_item.pest"]
pub struct ItemParser;

pub fn parse(s: &str) -> Result<Expandable, ParseError> {
	let span = tracing::debug_span!("custom_parse");
	let _guard = span.enter();

	let mut items = Vec::new();

	let mut pos = 0usize;
	while pos < s.len() {
		tracing::trace!(pos=%pos, rem=?&s[pos..]);
		if let Some(ps_start) = crate::utils::find_escapable_u8(&s[pos..], b'{', b'\\') {
			// convert the relative start position into an absolute one
			let ps_start = pos + ps_start;
			// push the text between the last parsed item and this one onto the list but
			// only if its len is >0
			if pos < ps_start {
				items.push(ExpandItem::Text { content: String::from(&s[pos..ps_start]) });
			}

			let ps_end = match crate::utils::find_escapable_u8(&s[ps_start + 1..], b'}', b'\\') {
				Some(idx) => ps_start + idx + 1,
				None => return Err(ParseError::NoCloseBrace { offset: ps_start }),
			};

			// we are getting the index of the } brace, but we need to index past that to
			// get everything to parse properly
			tracing::trace!(start=%ps_start, end=%ps_end, data=&s[ps_start..=ps_end], "attempting to parse special");
			items.push(parse_special(&s[ps_start..=ps_end])?);
			pos = ps_end + 1;
		} else {
			// There is no more specials to concider, so we are just eating the rest as a
			// string
			items.push(ExpandItem::Text { content: String::from(&s[pos..]) });
			break;
		}
	}

	Ok(Expandable::from_parts(items))
}

fn parse_special(s: &str) -> Result<ExpandItem, ParseError> {
	use pest::error::ErrorVariant as PEV;
	let mut tokens = match ItemParser::parse(Rule::item, s) {
		Ok(tokens) => tokens,
		Err(why) => match why.variant {
			PEV::ParsingError { positives, negatives } => {
				return Err(ParseError::ParsingError {
					pos: positives,
					neg: negatives,
					location: why.location,
				})
			}
			PEV::CustomError { message } => {
				return Err(ParseError::PestCustom { message, location: why.location })
			}
		},
	};

	let slug = tokens.next().unwrap().into_inner().next().unwrap();
	let expanded_item = match slug.as_rule() {
		Rule::time => {
			let mut time_items = slug.into_inner();
			let one = time_items.next();
			let two = time_items.next();

			if one.is_none() {
				ExpandItem::Time { zone: None }
			} else {
				let tz_str = format!(
					"{}{}",
					one.map(|el| el.as_str()).unwrap_or(""),
					two.map(|el| el.as_str()).unwrap_or("")
				);
				let tz = match crate::utils::parse_timezone(&tz_str) {
					Some(res) => res.0,
					None => return Err(ParseError::InvalidTimeZone),
				};

				ExpandItem::Time { zone: Some(tz) }
			}
		}
		Rule::caller => ExpandItem::Caller,
		Rule::arg => {
			let idx = slug
				.into_inner()
				.next()
				.expect("expected required arg idx for 'arg' but recieved `None`")
				.as_str()
				.parse::<usize>()
				.expect("the parser rules accepted invalid characters as a number");
			ExpandItem::Arg { idx }
		}
		rule => unreachable!("the rule {:?} was added but not implemented. this is a bug.", rule),
	};

	Ok(expanded_item)
}

#[derive(Debug, thiserror::Error)]
pub enum ParseError {
	#[error("missing closing brace for item at {}", .offset)]
	NoCloseBrace { offset: usize },
	#[error("failed to parse item")]
	ParsingError { location: Location, pos: Vec<Rule>, neg: Vec<Rule> },
	#[error("custom: {}", .message)]
	PestCustom { location: Location, message: String },
	#[error("the timezone passed is invalid")]
	InvalidTimeZone,
	#[error("you cannot use an empty string")]
	Empty,
	#[error("unknown error")]
	Unknown,
}
