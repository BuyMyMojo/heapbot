//! Database adapter for the bot.
//!
//! At the moment it uses a file-based approach. the structure is this
//!
//! ```
//! database/
//! ├── <channel name>/
//! │   ├── GENERAL_SETTINGS.json
//! │   ├── <plugin 1>.json
//! │   ├── <plugin 2>.json
//! │   └── <plugin 3>.json
//! └── .store_version
//! ```

use crate::{bot::Plugin, model::Channel};
use serde::{de::DeserializeOwned, Serialize};
use std::{
	collections::{BTreeSet, HashMap},
	io,
	io::ErrorKind as IoErrorKind,
	path::PathBuf,
};
use tokio::{
	fs,
	io::{AsyncBufReadExt as _, AsyncWriteExt, BufReader},
};

/// The file relative to the store root where the version is stored
static VERSION_FILE: &str = ".store_version";
const DB_VERSION: (u8, u8, u8) = (1, 0, 0);

/// Errors that can appear during db creation or initialization
#[derive(thiserror::Error, Debug)]
pub enum DBError {
	/// The filesystem reported the store path inaccessable
	#[error("unable to access store path")]
	FsPermissionDenied,
	/// The filesystem reported that the store path doesnt exist
	#[error("unable to find store path, was db ever initialized?")]
	StoreNotFound,
	/// The version in the store path is different than the one recognized by
	/// the program, maybe there is a migration script to convert to the new
	/// version?
	#[error(
		"store version mismatch: Ours = {}.{}.{}, Store = {}.{}.{}",
		.ours.0,   .ours.1,   .ours.2,
		.theirs.0, .theirs.1, .theirs.2
	)]
	StoreVersionMismatch { ours: (u8, u8, u8), theirs: (u8, u8, u8) },
	/// This is a unknown error
	#[error("unknown error")]
	Unknown,
}

pub struct DB {
	/// Path to the root of the db
	path: PathBuf,
	/// The version of this db
	store_version: (u8, u8, u8),
	/// Internal cache to speed up reads
	known: BTreeSet<Channel>,
}

impl DB {
	/// Create a new database at the specified path
	pub fn new(pth: PathBuf) -> DB {
		DB { path: pth, store_version: DB_VERSION, known: BTreeSet::new() }
	}

	/// Initialize the database
	pub async fn init(&mut self) -> Result<(), DBError> {
		let span = tracing::debug_span!("init", path=?self.path);
		let _guard = span.enter();

		// Initialize the store path if we havent already
		if !self.path.exists() {
			return self.init_store().await;
		}

		let version_path = self.path.join(VERSION_FILE);

		let fs_version = if !version_path.exists() {
			tracing::warn!("version file not found, assuming 0.0.0");
			(0, 0, 0)
		} else {
			let ver_f = match fs::File::open(&version_path).await {
				Ok(file) => BufReader::new(file),
				Err(e) => match e.kind() {
					IoErrorKind::PermissionDenied => {
						let re = DBError::FsPermissionDenied;
						tracing::warn!(error=?re, "unable to open version file: {}", re);
						return Err(re);
					}
					_ => {
						tracing::warn!(error=?e, "unhandled io errror!");
						return Err(DBError::Unknown);
					}
				},
			};

			let mut iter = ver_f.lines();

			// we are forcing the unparsables to 0 and also log it
			let _fn_unwrap_or_zero_logged = || {
				tracing::warn!("unable to parse version part, assuming 0");
				0
			};
			let maj = iter
				.next_line()
				.await
				.ok()
				.and_then(|v| v.unwrap().parse().ok())
				.unwrap_or_else(_fn_unwrap_or_zero_logged);
			let min = iter
				.next_line()
				.await
				.ok()
				.and_then(|v| v.unwrap().parse().ok())
				.unwrap_or_else(_fn_unwrap_or_zero_logged);
			let pat = iter
				.next_line()
				.await
				.ok()
				.and_then(|v| v.unwrap().parse().ok())
				.unwrap_or_else(_fn_unwrap_or_zero_logged);

			tracing::debug!(maj=%maj, min=%min, pat=%pat, "parsed version");

			(maj, min, pat)
		};

		if self.store_version != fs_version {
			let se = DBError::StoreVersionMismatch { ours: self.store_version, theirs: fs_version };
			tracing::info!(ours=?self.store_version, theirs=?fs_version, "{}", se);
			return Err(se);
		}

		Self::read_known_channels(&mut self.known, &self.path).await?;

		Ok(())
	}

	/// Read all toplevel directories from the given path
	async fn read_known_channels(ch: &mut BTreeSet<Channel>, pth: &PathBuf) -> Result<(), DBError> {
		let span = tracing::trace_span!("read_known_channels");
		let _guard = span.enter();

		let mut rdir = match fs::read_dir(pth).await {
			Ok(rd) => rd,
			Err(_e) => {
				tracing::warn!("unable to read store dir");
				return Ok(());
			}
		};

		while let Ok(Some(next)) = rdir.next_entry().await {
			match next.file_type().await {
				Ok(fi) => {
					if !fi.is_dir() {
						continue;
					}
				}
				Err(why) => {
					tracing::warn!(error=?why, "unable to get ftype information");
					continue;
				}
			}

			tracing::trace!(d=?next.path(), "found dir");

			// find the first "normal" component from the right side, and insert it.
			// as we check above that it is a directory, the last part should be the
			// directory name
			if let Some(std::path::Component::Normal(item)) =
				next.path().components().rfind(|el| matches!(el, std::path::Component::Normal(_)))
			{
				// we can use unwrap here, as dir iterator should *never* be able to produce
				// empty elements
				ch.insert(Channel::new(&item.to_string_lossy()).unwrap());
			}
		}

		Ok(())
	}

	/// Internal function to write all required files to the store
	async fn init_store(&self) -> Result<(), DBError> {
		tracing::info!("db path doesnt exist, attempting to create");
		if let Err(e) = fs::create_dir_all(&self.path).await {
			match e.kind() {
				IoErrorKind::PermissionDenied => {
					let re = DBError::FsPermissionDenied;
					tracing::warn!(error=?re, "unable to create store path: {}", re);
					return Err(re);
				}
				_ => {
					tracing::warn!(error=?e, "unhandled io errror!");
					return Err(DBError::Unknown);
				}
			}
		}

		tracing::debug!(version=?self.store_version, "writing store version");
		let mut f = match fs::File::create(self.path.join(VERSION_FILE)).await {
			Ok(file) => file,
			Err(e) => match e.kind() {
				IoErrorKind::PermissionDenied => {
					let re = DBError::FsPermissionDenied;
					tracing::warn!(error=?re, "failed to create store: {}", re);
					return Err(re);
				}
				IoErrorKind::NotFound => {
					let re = DBError::StoreNotFound;
					tracing::warn!(error=?re, "{}", re);
					return Err(re);
				}
				_ => {
					tracing::warn!(error=?e, "unhandled io errror!");
					return Err(DBError::Unknown);
				}
			},
		};

		if let Err(e) = f
			.write_all(
				&format!(
					"{}\n{}\n{}",
					self.store_version.0, self.store_version.1, self.store_version.2
				)
				.bytes()
				.collect::<Vec<_>>(),
			)
			.await
		{
			tracing::error!(error=?e, "unable to write store version");
			return Err(DBError::Unknown);
		}

		Ok(())
	}

	/// Store a plugin configuration for the given channel
	pub async fn store<PLG, CFG>(
		&mut self,
		plug_ident: &PLG,
		channel: &Channel,
		data: &CFG,
	) -> Result<(), StoreError>
	where
		PLG: Plugin,
		CFG: Serialize,
	{
		if !self.known.contains(channel) {
			fs::create_dir(self.path.join(channel.as_str_clean())).await?;
			self.known.insert(channel.clone());
		}

		let pth =
			self.path.join(channel.as_str_clean()).join(&format!("{}.json", plug_ident.name()));
		let f = fs::File::create(&pth).await?.into_std().await;
		serde_json::to_writer(f, &data)?;
		Ok(())
	}

	/// Looad a plugin configuration for the given channel
	pub async fn load<PLG, CFG>(
		&self,
		plug_ident: &PLG,
		channel: &Channel,
	) -> Result<Option<CFG>, LoadError>
	where
		PLG: Plugin,
		CFG: DeserializeOwned,
	{
		// here we dont need to check the known list as this will fail both when the
		// channel doesnt exist and the config doesnt exist
		let pth =
			self.path.join(channel.as_str_clean()).join(&format!("{}.json", plug_ident.name()));
		if !pth.exists() {
			return Ok(None);
		}
		let f = fs::File::open(&pth).await?.into_std().await;
		let data = serde_json::from_reader(f)?;
		Ok(Some(data))
	}

	/// Load the configuration for the given plugin for *all* known channels
	pub async fn load_known<PLG, CFG>(
		&mut self,
		plug_ident: &PLG,
	) -> Result<HashMap<Channel, CFG>, LoadError>
	where
		PLG: Plugin,
		CFG: DeserializeOwned,
	{
		let mut out = HashMap::new();
		for chan in &self.known {
			match self.load(plug_ident, &chan).await {
				Ok(Some(item)) => {
					let _ = out.insert(chan.clone(), item);
				}
				Ok(None) => continue,
				Err(why) => {
					tracing::warn!(
						"failed to load info for {}->{}",
						plug_ident.name(),
						chan.as_str()
					);
					return Err(why);
				}
			}
		}

		Ok(out)
	}

	pub fn base_dir(&self) -> &PathBuf {
		&self.path
	}

	/// Get a directory to put "special" content into
	pub async fn get_special(&self, name: &str) -> io::Result<PathBuf> {
		let p = self.path.join("_special").join(name);
		if !p.exists() {
			fs::create_dir_all(&p).await?;
		}
		Ok(p.to_path_buf())
	}
}

/// Errors that can appear during store operations
#[derive(thiserror::Error, Debug)]
pub enum StoreError {
	#[error("io error while storing data: {source:?}")]
	IO {
		#[from]
		source: io::Error,
	},

	#[error("unable to serialize data: {source:?}")]
	Serialize {
		#[from]
		source: serde_json::Error,
	},
}

/// Erros that can appear during load operations
#[derive(thiserror::Error, Debug)]
pub enum LoadError {
	#[error("io error while storing data: {source:?}")]
	IO {
		#[from]
		source: io::Error,
	},

	#[error("unable to deserialize data: {source:?}")]
	Deserialize {
		#[from]
		source: serde_json::Error,
	},
}
