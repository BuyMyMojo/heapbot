/// Connection info used to connect to the irc
pub struct TIRCConnectionInfo {
	pub endpoint: String,
	pub port: u16,
	pub user: String,
	pub oauth: String,
}

impl Default for TIRCConnectionInfo {
	fn default() -> Self {
		Self {
			endpoint: String::from("irc.chat.twitch.tv"),
			port: 6697,
			user: String::from("justinfan1337"),
			oauth: String::from("oauth:1234"),
		}
	}
}
