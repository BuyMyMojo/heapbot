use crate::{bot::Plugin, model::Flags};
use serde::{Deserialize, Serialize};

/// General settings, automatically retrievable for every channel by every
/// plugin
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GeneralSettings {
	pub flags: Flags,
}

impl Default for GeneralSettings {
	fn default() -> Self {
		Self { flags: Flags::default() }
	}
}

pub struct GeneralSettingsMarker;

impl Plugin for GeneralSettingsMarker {
	fn priority(&self) -> usize {
		999_999_999_999
	}
	fn name(&self) -> &'static str {
		"GENERAL_SETTINGS"
	}
}
