use chrono::{DateTime, Utc};
use once_cell::sync::Lazy;
use serde::{Deserialize, Serialize};

static CHARSET: Lazy<Vec<char>> =
	Lazy::new(|| "abcdefghijklmnopqrstuvwxyz0123456789".chars().collect::<Vec<_>>());
const LENGTH: usize = 4;

/// A quote
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Quote {
	/// Id of the quote, duplicated into the hashmap keys?
	id: String,
	/// Text of the quote
	text: String,
	/// Username at that time of the person quotes (there is no updating
	/// whatsoever)
	quoter: String,
	/// Game that was played when this quote was taken
	game: Option<String>,
	/// Time when the quote was created
	time: DateTime<Utc>,
}

impl Quote {
	/// Instanciate a new quite
	#[allow(dead_code)]
	pub fn new(
		id: String,
		text: String,
		quoter: String,
		game: Option<String>,
		time: DateTime<Utc>,
	) -> Quote {
		Quote { id, text, quoter, game, time }
	}

	/// Create a new quote with a random id and the time set to now
	pub fn create(text: String, quoter: String, game: Option<String>) -> Quote {
		Quote {
			id: crate::utils::gen_rand_id(&CHARSET, LENGTH),
			text,
			quoter,
			game,
			time: Utc::now(),
		}
	}

	/// create a stringy version of the quote
	pub fn stringify(&self, id: bool, time: bool, game: bool) -> String {
		let mut out = String::new();

		out.push_str(&self.text);
		if id || time || game {
			out.push(' ');
			out.push('[');

			if id {
				out.push_str(&format!("#{}", self.id));
				if time || game {
					out.push(' ');
				}
			}

			if time {
				out.push_str(&format!("{}", self.time.format("%F %R")));
				if game {
					out.push(' ')
				}
			}

			if let (true, Some(gam)) = (game, &self.game) {
				out.push_str(&gam);
			}

			out.push(']')
		}

		out
	}
}
