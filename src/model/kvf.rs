// use std::{
// 	fs::{File, OpenOptions},
// 	io::{BufRead, BufReader, ErrorKind as IoEKind, Write},
// 	path::PathBuf,
// };

use std::{io::ErrorKind as IoEKind, path::PathBuf};
use tokio::{
	fs::{File, OpenOptions},
	io::{AsyncBufReadExt, AsyncWriteExt, BufReader},
	stream::StreamExt,
};

/// A dead simple file-based key value store
#[derive(Debug)]
pub struct KvfDb {
	fs: PathBuf,
}

impl KvfDb {
	/// Create a new key-value store
	pub fn new<F: Into<PathBuf>>(p: F) -> KvfDb {
		KvfDb { fs: p.into() }
	}

	/// Find `needle` in the kv store
	#[tracing::instrument]
	pub async fn find(&self, needle: &str) -> Option<String> {
		tracing::trace!(needle=%needle, "attempting to load value from kv store");
		let file = match File::open(&self.fs).await {
			Ok(file) => file,
			Err(why) => {
				match why.kind() {
					IoEKind::NotFound => tracing::trace!("kv file not found"),
					_ => tracing::warn!(error=?why, "kv file open error"),
				}
				return None;
			}
		};
		let reader = BufReader::new(file);

		let needle = needle.replace('\t', " ");
		let mut lines = reader.lines().filter_map(|el| el.ok());
		while let Some(line) = lines.next().await {
			let mut parts = line.splitn(2, '\t');
			if parts.next() == Some(&needle.as_str()) {
				tracing::trace!("needle found");
				return parts.next().map(String::from);
			}
		}
		None
	}

	/// Add a new entry
	#[tracing::instrument]
	pub async fn add(&self, needle: &str, cow: &str) -> bool {
		let mut file = match OpenOptions::new().append(true).create(true).open(&self.fs).await {
			Ok(f) => f,
			Err(why) => {
				tracing::warn!("failed to store kv: {:?}", why);
				return false;
			}
		};

		match file.write_all(format!("{}\t{}\n", needle, cow).as_bytes()).await {
			Ok(_) => {
				tracing::trace!("written value to kv store");
				true
			}
			Err(why) => {
				tracing::warn!(error=?why, "failed to write value to kv store");
				false
			}
		}
	}
}
