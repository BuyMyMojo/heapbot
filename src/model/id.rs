//! Newtype wrappers around string, representing the different id types
// TODO: Add specialized HTTP methods for retrieving info from HELIX

#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct UserId(String);

#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct ChannelId(String);

#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct MessageId(String);

macro_rules! impl_id {
	($($e:ident),*) => {
		$(
			impl From<&str> for $e {
				fn from(o: &str) -> $e {
					$e(String::from(o))
				}
			}

			impl From<String> for $e {
				fn from(o: String) -> $e {
					$e(o)
				}
			}

			impl Into<String> for $e {
				fn into(self) -> String {
					self.0
				}
			}

			impl Default for $e {
				fn default() -> $e {
					$e(String::default())
				}
			}

			impl $e {
				pub fn as_str(&self) -> &str {
					&self.0
				}
			}
		)*
	}
}

impl_id!(UserId, ChannelId, MessageId);
