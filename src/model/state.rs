//! The bots 'state'.
//! This structure is passed to every invocation of the bot functions via
//! mutable reference!

use crate::{
	bot::Plugin,
	config::App,
	db::DB,
	model::{
		bot::{Command, CurrentUser},
		Channel, Flags, GeneralSettings, GeneralSettingsMarker, KvfDb, Response, RlSender,
	},
};
use reqwest::Client;
use std::{
	collections::{BTreeMap, HashMap},
	sync::Arc,
};
use tokio::sync::mpsc::error::SendError;
use typemap::SendMap;

/// General state for the bot. This is passed to all of the commands
pub struct State {
	/// Database, can be used to store plugin specific configuration
	pub db: DB,
	/// Shared Data
	pub data: SendMap,
	/// Request client for external resources
	http: Arc<Client>,
	/// Ratelimited Sender, used for outbound messages
	snd: RlSender,
	/// Cached general settings (settings common to all plugins)
	gens: HashMap<Channel, GeneralSettings>,
	/// A list of known command names, and their corresponding plugin
	commands: BTreeMap<String, Command>,
	/// Current User
	current_user: CurrentUser,
	/// Key Value Stores
	kvs: HashMap<String, KvfDb>,
	/// Api Info
	api: App,
}

impl State {
	/// Create a new state
	pub fn new(db: DB, snd: RlSender, cusr: CurrentUser, tw_creds: App) -> State {
		State {
			db,
			http: Arc::new(Self::create_client()),
			snd,
			gens: HashMap::new(),
			commands: BTreeMap::new(),
			current_user: cusr,
			kvs: HashMap::new(),
			api: tw_creds,
			data: SendMap::custom(),
		}
	}

	/// Retrieve a `GeneralSettings` object from the database
	pub async fn get_channel_cfg(&mut self, ch: &Channel) -> Option<&mut GeneralSettings> {
		if !self.gens.contains_key(ch) {
			match self.db.load::<_, GeneralSettings>(&GeneralSettingsMarker, ch).await {
				Ok(Some(l)) => {
					self.gens.insert(ch.clone(), l);
				}
				Ok(None) => {
					self.gens.insert(ch.clone(), GeneralSettings::default());
				}
				Err(why) => {
					tracing::error!(channel=%ch, error=?why, "failed to load general settings");
					return None;
				}
			}
		}

		self.gens.get_mut(ch)
	}

	/// Update a `GeneralSettings` object on disk, after modification
	pub async fn store_channel_cfg(&mut self, gs: &Channel) {
		if let Some(gset) = self.gens.get(gs) {
			match self.db.store(&GeneralSettingsMarker, gs, gset).await {
				Ok(()) => (),
				Err(why) => {
					tracing::error!(channel=%gs, error=?why, "failed to store general settings");
				}
			}
		}
	}

	/// Get a command if it exists
	pub fn get_command(&self, name: &str) -> Option<&Command> {
		self.commands.get(name)
	}

	/// Register a command new command name with the bot. A command name has to
	/// be unique. If it already has been registered or the name is empty, this
	/// function returns false.
	pub fn register_command<PLG>(&mut self, plg_info: &PLG, name: &str) -> bool
	where
		PLG: Plugin,
	{
		if !name.is_empty() && !self.commands.contains_key(name) {
			self.commands.insert(name.to_owned(), Command::new(name, plg_info.name()));
			return true;
		}
		false
	}

	/// Remove a command
	pub fn unregister_command(&mut self, name: &str) -> Option<Command> {
		self.commands.remove(name)
	}

	/// Remove all the commands for a plugin
	pub fn purge_commands<PLG: Plugin>(&mut self, plg_info: &PLG) -> usize {
		let mut to_remove = Vec::new();
		let mut count = 0;
		let mark = plg_info.name();

		for (k, v) in &self.commands {
			if v.ns() == mark {
				to_remove.push(k.clone());
			}
		}

		for r in to_remove {
			if self.commands.remove(&r).is_some() {
				count += 1;
			}
		}

		count
	}

	/// Send a message via the ratelimited sender
	/// this should only be called by the Respond::send method
	pub(in crate::model) async fn send(
		&mut self,
		value: Response,
	) -> Result<bool, SendError<Response>> {
		let is_nolimit = match self.get_channel_cfg(&value.target).await {
			Some(sett) => sett.flags.contains(Flags::NO_LIMIT),
			None => false,
		};

		self.snd.send(is_nolimit, value).await
	}

	/// Mark the sender as if the bot user has sent a message, without actually
	/// sending one
	pub fn mark_send(&mut self, tgt: &Channel) {
		self.snd.take_only(tgt);
	}

	/// Get the current user of the bot
	pub fn current_user(&self) -> &CurrentUser {
		&self.current_user
	}

	/// Access the http client
	pub fn http(&self) -> Arc<Client> {
		Arc::clone(&self.http)
	}

	/// Get a named kv store
	pub fn kv(&mut self, name: &str) -> &KvfDb {
		if !self.kvs.contains_key(name) {
			self.kvs.insert(
				name.to_owned(),
				KvfDb::new(self.db.base_dir().join(&format!("{}.kv", name))),
			);
		}

		self.kvs.get(name).unwrap()
	}

	pub fn get_api_creds(&self) -> &App {
		&self.api
	}

	/// Internal function to create the http client returned by Self::http()
	fn create_client() -> Client {
		use reqwest::{header as H, header::HeaderMap};

		let mut hmp = HeaderMap::new();
		hmp.insert(H::DNT, 1.into());
		hmp.insert(H::UPGRADE_INSECURE_REQUESTS, 1.into());

		Client::builder()
			.gzip(true)
			.user_agent(concat!(
				"twitch bot (HeapBot v",
				env!("CARGO_PKG_VERSION"),
				" https://git.sr.ht/~heapunderflow/pogey)"
			))
			.default_headers(hmp)
			.build()
			.expect("Failed to create client")
	}
}
