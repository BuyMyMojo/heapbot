//! This is a incredibly naive implementation of a ratelimited sender
//! if the bot is in more of a few channel, a better implementation has to be
//! made or chosen

use crate::model::{Channel, Response};
use chrono::{DateTime, Duration, Utc};
use once_cell::sync::OnceCell;
use std::collections::HashMap;
use tokio::sync::mpsc::{error::SendError, Sender};

/// Ratelimited Sender
/// (basically a wrapper around a tokio::sync::mpsc::Sender)
pub struct RlSender {
	snd: Sender<Response>,
	rlopt: RlOpts,
	rlstate: RlState,
}

impl RlSender {
	pub fn new(snd: Sender<Response>, opt: RlOpts) -> RlSender {
		RlSender { snd, rlopt: opt, rlstate: RlState::new() }
	}

	pub async fn send(
		&mut self,
		override_rl: bool,
		value: Response,
	) -> Result<bool, SendError<Response>> {
		if !override_rl && !self.rlstate.check_next(&value.target, self.rlopt.delay()) {
			return Ok(false);
		}

		self.rlstate.take_next(&value.target);
		self.snd.send(value).await?;
		Ok(true)
	}

	pub fn take_only(&mut self, tgt: &Channel) {
		self.rlstate.take_next(tgt);
	}
}

/// Utility to keep track of the current ratelimiting state
#[derive(Debug)]
struct RlState {
	last: HashMap<Channel, DateTime<Utc>>,
}

impl RlState {
	/// Create a new state with the last time set to now
	#[allow(dead_code)]
	pub fn new() -> RlState {
		RlState { last: HashMap::new() }
	}

	/// Check if the next message can be sent (`delay` has passed since the last
	/// take)
	pub fn check_next(&mut self, chn: &Channel, delay: u64) -> bool {
		// If we find the channel in the list, check if the timeout applies,
		// if we DIDNT find the channel in the list, assume we can send a message
		match self.last.get(chn) {
			Some(time) => *time + Duration::milliseconds(delay as i64) < Utc::now(),
			None => {
				self.last
					.insert(chn.clone(), Utc::now() - Duration::milliseconds(delay as i64 - 1));
				true
			}
		}
	}

	/// Set the last taken to 'now'
	pub fn take_next(&mut self, chn: &Channel) {
		match self.last.get_mut(chn) {
			Some(ch) => *ch = Utc::now(),
			None => {
				self.last.insert(chn.clone(), Utc::now());
			}
		}
	}
}

/// Options for ratelimiting
#[derive(Debug, Clone)]
pub struct RlOpts {
	num_output: u32,
	per_ms: u64,
	delay: OnceCell<u64>,
}

impl RlOpts {
	/// c amount of messages per t milliseconds
	pub fn new(c: u32, t: u64) -> RlOpts {
		RlOpts { num_output: c, per_ms: t, delay: OnceCell::new() }
	}

	pub fn num(&self) -> u64 {
		self.num_output as u64
	}

	pub fn time(&self) -> u64 {
		self.per_ms
	}

	pub fn delay(&self) -> u64 {
		// we are computing the delay here once, and then just copy it every time we
		// need it
		*self.delay.get_or_init(|| (self.time() as f64 / self.num() as f64).floor() as u64)
	}
}
