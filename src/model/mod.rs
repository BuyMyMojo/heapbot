//! Models and structs used by the plugins and the bot itself

pub mod bot;
pub mod twitch_api;

mod channel;
mod custom;
mod flags;
mod general;
mod id;
mod irc;
mod kvf;
mod message;
mod quote;
mod repeatable;
mod rl_sender;
mod state;

pub static OWNER: &str = "heapunderflow";
pub static OWNERID: &str = "38059093";

pub use self::{
	channel::Channel,
	custom::{ExpandArgs, ExpandItem, Expandable},
	flags::{Flags, MessageFlags},
	general::{GeneralSettings, GeneralSettingsMarker},
	id::{ChannelId, MessageId, UserId},
	irc::TIRCConnectionInfo,
	kvf::KvfDb,
	message::{Message, Response},
	quote::Quote,
	repeatable::Repeatable,
	rl_sender::{RlOpts, RlSender},
	state::State,
};
