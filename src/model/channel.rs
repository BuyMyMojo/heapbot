use std::fmt;

/// Represents a twitch channel
#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct Channel(String);

impl fmt::Display for Channel {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "{}", &self.0[1..])
	}
}

impl Channel {
	/// Create a new channel object.
	/// Returns `None` if the channel name is empty
	pub fn new(ch: &str) -> Option<Channel> {
		if ch.starts_with('#') && ch.len() > 1 {
			return Some(Channel(String::from(ch)));
		}

		if !ch.is_empty() {
			return Some(Channel(format!("#{}", ch)));
		}

		None
	}

	/// Get the channel name (including the leading #)
	pub fn as_str(&self) -> &str {
		&self.0
	}

	/// Get the channel name (excliding the leading #)
	pub fn as_str_clean(&self) -> &str {
		&self.0[1..]
	}
}
