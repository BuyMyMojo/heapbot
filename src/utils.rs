use chrono::FixedOffset;
use pest::error::InputLocation;
use rand::Rng;
use std::fmt;

/// Trim a suffix of a &str
#[inline]
pub fn trim_suffix<'t>(s: &'t str, sfx: &str) -> &'t str {
	if s.len() >= sfx.len() && s[(s.len() - sfx.len())..s.len()] == sfx[..] {
		&s[..(s.len() - sfx.len())]
	} else {
		&s
	}
}

/// generate a random string from a char list and a given length
#[inline]
pub fn gen_rand_id(alph: &[char], len: usize) -> String {
	let mut out = String::with_capacity(len);
	let mut rng = rand::thread_rng();
	for _ in 0..len {
		out.push(alph[rng.gen_range(0, alph.len())]);
	}
	out
}

// With this i can stringify spans into oneline error messages
// usable for twitch
/// Convert a pest::error::InputLocation to String
#[inline]
pub fn pest_location_to_string(loc: InputLocation) -> String {
	match loc {
		InputLocation::Pos(pos) => format!("Col {}", pos),
		InputLocation::Span((l, r)) => format!("Col {}-{}", l, r),
	}
}

/// Clamp a string to a byte length
#[inline]
pub fn clamp_str(s: &str, len: usize) -> &str {
	if s.len() < len {
		&s
	} else {
		&s[..len]
	}
}

/// Clamp a string to a length and add a elipsis if it was clamped
#[inline]
pub fn elipsis(s: &str, len: usize) -> String {
	if s.len() > len {
		format!("{}...", &s[..len])
	} else {
		s.to_owned()
	}
}

/// Clamps a sortable value between to other values
#[inline]
pub fn clamp2<T: PartialOrd>(v: T, l: T, r: T) -> T {
	if v < l {
		l
	} else if v > r {
		r
	} else {
		v
	}
}

#[inline]
pub fn find_escapable_u8(s: &str, find: u8, escape: u8) -> Option<usize> {
	if s.is_empty() {
		return None;
	}
	let bytes = s.as_bytes();
	let mut idx = 0;
	let mut is_escaping = false;
	while idx < s.len() {
		if !is_escaping && bytes[idx] == find {
			return Some(idx);
		} else if bytes[idx] == escape {
			is_escaping = !is_escaping;
		}
		idx += 1;
	}
	None
}

// Parse and clamp a timezone
pub fn parse_timezone(s: &str) -> Option<(i32, FixedOffset)> {
	let mut val = s.parse::<i32>().ok()?;
	val = clamp2(val, -12, 13);
	Some((val, FixedOffset::east_opt(val * 3600)?))
}

/// "Literally Debug"
/// When the debug implementation of this is called, it prints out the
/// format args given LITERALLY to the output stream
/// (there will be no quoting, newlines, intelligent wrapping whatsoever
/// internally)
pub struct DebugLit<'t>(pub fmt::Arguments<'t>);
impl<'t> fmt::Debug for DebugLit<'t> {
	fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
		fmt::write(f, self.0)
	}
}

#[cfg(test)]
mod test {
	use super::{
		clamp2, clamp_str as _clamp_str, elipsis as _elipsis, find_escapable_u8, gen_rand_id,
		pest_location_to_string, trim_suffix,
	};

	#[test]
	fn trim_suffix_ltlength() {
		assert_eq!(".123", trim_suffix(".123", ".1234"));
	}

	#[test]
	fn trim_suffix_eqlength() {
		assert_eq!("", trim_suffix(".1234", ".1234"));
	}

	#[test]
	fn trim_suffix_gtlength() {
		assert_eq!("abc", trim_suffix("abc.123", ".123"));
	}

	#[test]
	fn gen_rand_id_len() {
		assert!(gen_rand_id(&['a', 'b', 'c'], 5).len() == 5);
	}

	#[test]
	fn gen_rand_id_alphabet() {
		assert!(gen_rand_id(&['a', 'b'], 100).chars().all(|e| e == 'a' || e == 'b'));
	}

	#[test]
	fn pest_loc_pos() {
		use pest::error::InputLocation;
		assert_eq!(String::from("Col 13"), pest_location_to_string(InputLocation::Pos(13)));
	}

	#[test]
	fn pest_loc_span() {
		use pest::error::InputLocation;
		assert_eq!(
			String::from("Col 10-20"),
			pest_location_to_string(InputLocation::Span((10, 20)))
		);
	}

	#[test]
	fn clamp2_all() {
		assert_eq!(0, clamp2(-100, 0, 50));
		assert_eq!(50, clamp2(100, 0, 50));
		assert_eq!(25, clamp2(25, 0, 50));
	}

	#[test]
	fn clamp_str() {
		assert_eq!("1234", _clamp_str("1234", 5));
		assert_eq!("1234", _clamp_str("1234", 4));
		assert_eq!("12", _clamp_str("1234", 2));
	}

	#[test]
	fn elipsis() {
		assert_eq!("1234", _elipsis("1234", 5));
		assert_eq!("1234", _elipsis("1234", 4));
		assert_eq!("12...", _elipsis("1234", 2));
	}

	#[test]
	fn escapable_u8() {
		assert_eq!(None, find_escapable_u8("test { 123", b'}', b'\\'));
		assert_eq!(None, find_escapable_u8(r"test {\} 123", b'}', b'\\'));
		assert_eq!(Some(5), find_escapable_u8("test {} 123", b'{', b'\\'));
		assert_eq!(Some(0), find_escapable_u8("{}{}", b'{', b'\\'));
		assert_eq!(Some(4), find_escapable_u8("test}{}", b'}', b'\\'));
	}
}
