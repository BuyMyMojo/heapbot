use super::prelude::*;
use crate::{
	model::{Expandable, MessageFlags},
	parser::custom::ParseError,
};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Debug, Clone, Copy, Ord, PartialOrd, Eq, PartialEq, Hash, Serialize, Deserialize)]
#[serde(rename = "snake_case")]
pub enum Permission {
	Nothing,
	Subscriber,
	Moderator,
	Broadcaster,
	Owner,
}

impl From<MessageFlags> for Permission {
	fn from(other: MessageFlags) -> Permission {
		if other & MessageFlags::OWNER == MessageFlags::OWNER {
			return Permission::Owner;
		}
		if other & MessageFlags::BROADCASTER == MessageFlags::BROADCASTER {
			return Permission::Broadcaster;
		}
		if other & MessageFlags::MODERATOR == MessageFlags::MODERATOR {
			return Permission::Moderator;
		}
		if other & MessageFlags::SUBSCRIBER == MessageFlags::SUBSCRIBER {
			return Permission::Subscriber;
		}
		Permission::Nothing
	}
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CustomCommand {
	#[serde(rename = "i")]
	item: Expandable,
	#[serde(rename = "p")]
	permissions: Permission,
}

impl CustomCommand {
	pub fn expandable(&self) -> &Expandable {
		&self.item
	}
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct CustomState {
	commands: HashMap<String, CustomCommand>,
}

pub struct CustomCommandPlugin {
	states: HashMap<Channel, CustomState>,
}

impl Default for CustomCommandPlugin {
	fn default() -> Self {
		Self { states: HashMap::default() }
	}
}

impl CustomCommandPlugin {
	async fn cmd_addcustom(&mut self, global_state: &mut State, msg: &Message, rem: &str) -> bool {
		if !msg.flags.is_privileged() {
			return false;
		}

		let state = self.states.entry(msg.target.clone()).or_default();
		let mut parts = rem.split(' ').peekable();

		let name = match parts.next() {
			Some(name) => name,
			None => {
				tracing::debug!("missing name");
				err_log!(
					msg.respond(String::from("how dare you! i dont accept unnamed commands"))
						.send(global_state)
						.await
				);
				return false;
			}
		};

		if state.commands.contains_key(name) {
			tracing::trace!(k=?name, "command exists");
			err_log!(
				msg.respond(String::from("a command with this name already exists"))
					.send(global_state)
					.await
			);
			return false;
		}

		// let opts = parts.by_ref().take_while(|el|
		// el.starts_with("-")).collect::<Vec<_>>();

		let mut opts = Vec::new();
		loop {
			if !parts.peek().map(|el| el.starts_with('-')).unwrap_or(false) {
				break;
			}
			match parts.next() {
				Some(item) => opts.push(item),
				None => break,
			}
		}

		tracing::trace!("parse expandable");
		let acc = parts.fold(String::new(), |mut acc, i| {
			acc.push_str(i);
			acc.push(' ');
			acc
		});
		let remainder = match Expandable::parse(acc.trim()) {
			Ok(exp) => exp,
			Err(why) => {
				match why {
					ParseError::NoCloseBrace { offset } => err_log!(
						msg.respond(format!("missing a closing brace at {}", offset))
							.send(global_state)
							.await
					),
					ParseError::ParsingError { location, pos, .. } => err_log!(
						msg.respond(format!(
							"failed to parse item at {}, expected one of: {}",
							crate::utils::pest_location_to_string(location),
							pos.iter().map(|el| format!("{:?}, ", el)).collect::<String>()
						))
						.send(global_state)
						.await
					),
					ParseError::PestCustom { location, message } => err_log!(
						msg.respond(format!(
							"error at {}: {}",
							crate::utils::pest_location_to_string(location),
							message
						))
						.send(global_state)
						.await
					),
					ParseError::InvalidTimeZone => {
						err_log!(msg.respond(format!("{}", why)).send(global_state).await)
					}
					ParseError::Empty => err_log!(
						msg.respond(String::from("you cannot set an empty command"))
							.send(global_state)
							.await
					),
					ParseError::Unknown => unreachable!("there should never be an unknown error"),
				}
				return false;
			}
		};

		let mut permissions = Permission::Nothing;
		tracing::debug_span!("parsing_ops").in_scope(|| {
			tracing::debug!("start");
			for opt in opts {
				// for now we only support the perm flags
				if opt.as_bytes().iter().any(|b| *b == b'=') {
					// we should always get 2 parts here as we check before hand that we have a =
					// sign
					let eq = opt.splitn(2, '=').collect::<Vec<_>>();
					tracing::trace!(v=?eq);
					debug_assert_eq!(2, eq.len(), "kv pair is not actually a pair");

					match (eq[0], eq[1]) {
						("-p", v) | ("-pem", v) | ("-permission", v) => match v {
							"sub" | "subscriber" => permissions = Permission::Subscriber,
							"mod" | "moderator" => permissions = Permission::Moderator,
							"brd" | "broadcaster" => permissions = Permission::Broadcaster,
							"own" | "owner" => permissions = Permission::Owner,
							_ => (),
						},
						_ => (),
					}
				} else {
					tracing::trace!(v=?opt);
					match opt {
						"-sub" | "-subscriber" => permissions = Permission::Subscriber,
						"-mod" | "-moderator" => permissions = Permission::Moderator,
						"-brd" | "-broadcaster" => permissions = Permission::Broadcaster,
						"-own" | "-owner" => permissions = Permission::Owner,
						_ => (),
					}
				}
			}
		});

		let cc = CustomCommand { item: remainder, permissions };
		tracing::trace!(cc=?cc, "new");
		state.commands.insert(name.to_owned(), cc);

		err_log!(msg.respond(format!("added custom command {}", name)).send(global_state).await);

		true
	}

	async fn cmd_delcustom(&mut self, global_state: &mut State, msg: &Message, rem: &str) -> bool {
		if !msg.flags.is_privileged() {
			return false;
		}

		let rem = rem.trim();

		let state = match self.states.get_mut(&msg.target) {
			Some(state) => state,
			None => {
				tracing::debug!("there are no custom commands");
				err_log!(
					msg.respond(String::from("yea nope, dont know about that one chief"))
						.send(global_state)
						.await
				);
				return false;
			}
		};

		if state.commands.remove(rem).is_some() {
			tracing::info!(cc=?rem, "removed custom command");
			err_log!(
				msg.respond(format!("removed custom command `{}`", rem)).send(global_state).await
			);
			true
		} else {
			err_log!(
				msg.respond(format!("unable to find custom command `{}`", rem))
					.send(global_state)
					.await
			);
			false
		}
	}
}

#[async_trait::async_trait]
impl Plugin for CustomCommandPlugin {
	async fn init(&mut self, global_state: &mut State) -> Result<(), String> {
		let load_state = match global_state.db.load_known::<_, CustomState>(self).await {
			Ok(states) => states,
			Err(error) => return Err(format!("{:?}", error)),
		};

		self.states = load_state;

		global_state.register_command(self, "addcom");
		global_state.register_command(self, "addcustom");
		global_state.register_command(self, "delcom");
		global_state.register_command(self, "delcustom");

		Ok(())
	}

	// we are gonna (ab)use on_message for finding our custom commands. we should
	// never be able to shadow builtins as on_command gets handled before on_message

	async fn on_message(&mut self, global_state: &mut State, msg: &Message) -> PlugResult {
		let state = match self.states.get(&msg.target) {
			Some(sta) => sta,
			None => {
				tracing::trace!("no state to match custom commands");
				return PlugResult::Continue;
			}
		};

		let parts: Vec<_> = msg.content.split(' ').collect();
		tracing::trace_span!("cc_find", name = &parts[0]);
		if let Some(com) = state.commands.get(parts[0]) {
			tracing::trace!("found");
			if com.permissions <= msg.flags.into() {
				tracing::trace!("perms match");
				// TODO: Expand
				let out = match com.expandable().expand(msg, &parts[1..]) {
					Ok(s) => s,
					Err(why) => format!("{}", why),
				};
				err_log!(msg.respond(out).send(global_state).await);
				return PlugResult::Stop;
			}
		}

		PlugResult::Continue
	}

	async fn on_command(
		&mut self,
		global_state: &mut State,
		msg: &Message,
		name: &str,
		rem: &str,
	) -> PlugResult {
		let dirty = match name {
			"addcom" | "addcustom" => self.cmd_addcustom(global_state, msg, rem).await,
			"delcom" | "delcustom" => self.cmd_delcustom(global_state, msg, rem).await,
			_ => {
				tracing::warn!(
					"we registered for the command {}, but it was never added to dispatch!",
					name
				);
				return PlugResult::Continue;
			}
		};

		if dirty {
			if let Some(state) = self.states.get(&msg.target) {
				if let Err(e) = global_state.db.store(&*self, &msg.target, &state).await {
					tracing::warn!(error=?e, "failed to store custom command state!");
				}
			}
		}

		PlugResult::Stop
	}

	fn priority(&self) -> usize {
		40
	}
	fn name(&self) -> &'static str {
		"custom_command"
	}
}
