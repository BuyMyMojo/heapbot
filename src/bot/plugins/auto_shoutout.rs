use super::prelude::*;
use chrono::{DateTime, Duration, Utc};
use once_cell::sync::Lazy;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

static LAST_SEEN_TIMEOUT_DURATION: Lazy<Duration> = Lazy::new(|| Duration::hours(2));

#[derive(Debug, Default)]
pub struct AutoShoutoutPlug {
	states: HashMap<Channel, SoChannel>,
}

type SoList = HashMap<String, String>;

#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub struct SoChannel {
	last_seen: HashMap<String, DateTime<Utc>>,
	so_list: SoList,
}

#[async_trait::async_trait]
impl Plugin for AutoShoutoutPlug {
	// ---------

	async fn init(&mut self, state: &mut State) -> Result<(), String> {
		let load_state = match state.db.load_known::<_, SoList>(self).await {
			Ok(states) => states,
			Err(error) => return Err(format!("{:?}", error)),
		};

		self.states = HashMap::with_capacity(load_state.len());
		for (k, v) in load_state {
			self.states.insert(k, SoChannel { last_seen: HashMap::new(), so_list: v });
		}

		// we are only registering a single command,
		// as we are going to use verbs
		state.register_command(self, "auto_shoutout");
		Ok(())
	}

	async fn on_message(&mut self, gstate: &mut State, msg: &Message) -> PlugResult {
		let state = match self.states.get_mut(&msg.target) {
			Some(state) => state,
			None => return PlugResult::Continue,
		};

		let now = Utc::now();

		let xm = msg.name.to_ascii_lowercase();
		if !state.so_list.contains_key(&xm) {
			return PlugResult::Continue;
		}

		if state
			.last_seen
			.get(&xm)
			.map(|e| (now - *e) > *LAST_SEEN_TIMEOUT_DURATION)
			.unwrap_or(true)
		{
			err_log!(msg.respond(state.so_list.get(&xm).unwrap()).send(gstate).await);
		}

		state.last_seen.insert(xm, now);
		PlugResult::Stop
	}

	async fn on_command(
		&mut self,
		global_state: &mut State,
		msg: &Message,
		name: &str,
		rest: &str,
	) -> PlugResult {
		if name != "auto_shoutout" {
			tracing::warn!(
				"we registered for the command {}, but it was never added to dispatch!",
				name
			);
			return PlugResult::Continue;
		}

		if !msg.flags.is_privileged() {
			return PlugResult::Continue;
		}

		let mut parts = rest.splitn(2, ' ');
		let verb = match parts.next() {
			Some(v) => v,
			None => {
				tracing::debug!("no verb supplied");
				return PlugResult::Stop;
			}
		};

		let rest = parts.collect::<String>();

		let dirty = match verb {
			"add" | "create" => self.verb_add(global_state, msg, &rest).await,
			"remove" | "delete" | "rm" => self.verb_remove(global_state, msg, &rest).await,
			"list" => self.verb_list(global_state, msg, &rest).await,
			"inspect" => self.verb_inspect(global_state, msg, &rest).await,
			_ => false,
		};

		if dirty {
			if let Some(state) = self.states.get(&msg.target) {
				if let Err(e) = global_state.db.store(&*self, &msg.target, &state.so_list).await {
					tracing::warn!(error=?e, "failed to store auto_so state!");
				}
			}
		}

		PlugResult::Stop
	}

	fn priority(&self) -> usize {
		30
	}
	fn name(&self) -> &'static str {
		"auto_so"
	}
}

impl AutoShoutoutPlug {
	async fn verb_add(&mut self, gstate: &mut State, msg: &Message, rest: &str) -> bool {
		let state = self.states.entry(msg.target.clone()).or_default();

		let mut parts = rest.splitn(2, ' ');
		let user = match parts.next() {
			Some(name) => name.to_ascii_lowercase(),
			None => {
				err_log!(msg.respond("missing username").send(gstate).await);
				return false;
			}
		};

		let message = match parts.next() {
			Some(message) => message,
			None => {
				err_log!(msg.respond("missing shoutout message").send(gstate).await);
				return false;
			}
		};

		let username = user.trim_start_matches('@');

		let act = match state.so_list.insert(username.to_owned(), message.to_owned()) {
			Some(_) => ("updated", "to"),
			None => ("added", "with"),
		};
		err_log!(
			msg.respond(format!(
				"{action} auto shoutout for {} {abind} message \"{}\"",
				username,
				message,
				action = act.0,
				abind = act.1
			))
			.send(gstate)
			.await
		);
		true
	}

	async fn verb_remove(&mut self, gstate: &mut State, msg: &Message, rest: &str) -> bool {
		let state = match self.states.get_mut(&msg.target) {
			Some(state) => state,
			None => {
				err_log!(
					msg.respond("you dont have any auto shoutouts setup yet").send(gstate).await
				);
				return false;
			}
		};

		if state.so_list.remove(rest.to_ascii_lowercase().trim_start_matches('@')).is_some() {
			err_log!(msg.respond(format!("removed auto shoutout for {}", rest)).send(gstate).await);
			true
		} else {
			err_log!(
				msg.respond(format!("cannot find auto shoutout for {}", rest)).send(gstate).await
			);
			false
		}
	}

	async fn verb_list(&mut self, gstate: &mut State, msg: &Message, _: &str) -> bool {
		let state = match self.states.get_mut(&msg.target) {
			Some(state) if !state.so_list.is_empty() => state,
			_ => {
				err_log!(
					msg.respond("you dont have any auto shoutouts setup yet").send(gstate).await
				);
				return false;
			}
		};

		let mut out = String::new();
		for so in state.so_list.keys() {
			out.push_str(&format!("{}, ", so));
		}

		err_log!(
			msg.respond(format!("AutoSO's listed: {}", out.trim_end_matches(", ")))
				.send(gstate)
				.await
		);
		false
	}

	async fn verb_inspect(&mut self, _gstate: &mut State, _msg: &Message, _rest: &str) -> bool {
		false
	}
}
