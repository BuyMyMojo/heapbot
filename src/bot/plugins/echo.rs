use super::prelude::*;

/// Echo plugin, echos events to the debug log channel, and messages recieved to
/// the trace log channel
pub struct EchoPlugin;

#[async_trait::async_trait]
impl Plugin for EchoPlugin {
	async fn init(&mut self, _: &mut State) -> Result<(), String> {
		tracing::debug!("echo plugin init called");
		Ok(())
	}

	async fn on_message(&mut self, _: &mut State, msg: &Message) -> PlugResult {
		tracing::debug!("echo plug recieved message");
		tracing::trace!("echo plug PRIVMSG:\n{:#?}", msg);
		PlugResult::Continue
	}

	async fn stop(&mut self, _: Option<&mut State>) {
		tracing::debug!("echo plugin stop called");
	}

	fn priority(&self) -> usize {
		0
	}

	fn name(&self) -> &'static str {
		"echo plug"
	}
}
