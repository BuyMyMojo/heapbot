//! Plugins

/// Prelude imports for commonly used types
pub(self) mod prelude {
	pub use crate::{
		bot::Plugin,
		model::{bot::PlugResult, Channel, Message, State},
	};
}

mod auto_shoutout;
mod custom;
mod echo;
mod quote;
mod repeater;
mod snugtroll;

pub use self::{
	auto_shoutout::AutoShoutoutPlug, custom::CustomCommandPlugin, echo::EchoPlugin,
	quote::QuotePlugin, repeater::RepeaterPlugin, snugtroll::SnugTrollPlug,
};
