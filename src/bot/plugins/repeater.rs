use super::prelude::*;
use crate::model::{Channel, Repeatable};
use aho_corasick::{AhoCorasick, AhoCorasickBuilder, MatchKind as AhoMatchKind};
use serde::{Deserialize, Serialize};
use std::collections::{BTreeMap, HashMap};

#[derive(Debug, Serialize, Deserialize)]
pub struct RplugState {
	reps: Vec<Repeatable>,
	counters: HashMap<String, u64>,
	no: bool,
}

impl Default for RplugState {
	fn default() -> RplugState {
		RplugState { reps: Vec::default(), counters: HashMap::default(), no: false }
	}
}

#[derive(Debug, Default)]
pub struct RepeaterPlugin {
	states: HashMap<Channel, RplugState>,
	matcher: BTreeMap<Channel, AhoCorasick>,
}

impl RepeaterPlugin {
	async fn cmd_addpogey(&mut self, glob_state: &mut State, msg: &Message, rem: &str) -> bool {
		if !msg.flags.is_privileged() {
			return false;
		}

		let rep = match Repeatable::parse(rem) {
			Ok(rep) => rep,
			Err(why) => {
				tracing::debug!("failed to parse repeatable: {}", why);
				err_log!(msg.respond(format!("Invalid Pogey: {}", why)).send(glob_state).await);
				return false;
			}
		};

		let state = self.states.entry(msg.target.clone()).or_default();
		state.reps.push(rep);

		self.matcher.insert(
			msg.target.clone(),
			AhoCorasickBuilder::new()
				.match_kind(AhoMatchKind::LeftmostFirst)
				.build(state.reps.iter().map(|el| el.trigger()).collect::<Vec<_>>()),
		);

		tracing::info!(repeatable=?state.reps[state.reps.len() - 1], "repeatable added");
		err_log!(
			msg.respond(format!(
				"added pogey {} [{} total]",
				state.reps[state.reps.len() - 1].trigger(),
				state.reps.len()
			))
			.send(glob_state)
			.await
		);

		true
	}

	async fn cmd_delpogey(&mut self, glob_state: &mut State, msg: &Message, rem: &str) -> bool {
		if !msg.flags.is_privileged() {
			return false;
		}

		let state = match self.states.get_mut(&msg.target) {
			Some(sta) => sta,
			None => {
				tracing::debug!("cannot remove pogey as there is literally no state on record");
				return false;
			}
		};

		let trimmed = rem.trim().splitn(2, ' ').next().unwrap_or("");

		let el = match state.reps.iter().enumerate().find(|(_, v)| v.trigger() == trimmed) {
			Some((idx, _)) => idx,
			None => {
				tracing::debug!("tried to delete {} but it was not found in the list", trimmed);
				err_log!(
					msg.respond(String::from("i cannot find that pogey")).send(glob_state).await
				);
				return false;
			}
		};

		let rem = state.reps.remove(el);

		tracing::info!(repeatable=?rem, "repeatable removed");
		err_log!(
			msg.respond(format!("removed pogey {} [{} left]", rem.trigger(), state.reps.len()))
				.send(glob_state)
				.await
		);
		true
	}

	async fn cmd_pogeys(&mut self, glob_state: &mut State, msg: &Message) -> bool {
		let state = match self.states.get(&msg.target) {
			Some(sta) => sta,
			None => {
				tracing::debug!("no stats yet");
				err_log!(
					msg.respond(String::from("i literally know nothing!")).send(glob_state).await
				);
				return false;
			}
		};

		if state.counters.is_empty() {
			err_log!(msg.respond(String::from("69 420")).send(glob_state).await);
			return false;
		}

		let mut toplist = state.counters.iter().collect::<Vec<_>>();
		toplist.sort_by(|left, right| right.1.cmp(left.1));

		let mut message = String::new();
		for top in toplist.iter().take(5) {
			message.push_str(&format!("{name} (x{count}) ", name = top.0, count = top.1));
		}
		err_log!(msg.respond(message).send(glob_state).await);
		false
	}

	async fn cmd_clearcount(&mut self, glob_state: &mut State, msg: &Message, _rem: &str) -> bool {
		if !msg.flags.is_privileged() {
			return false;
		}

		let state = match self.states.get_mut(&msg.target) {
			Some(state) => state,
			None => {
				tracing::debug!("there is nothing to reset");
				err_log!(
					msg.respond(String::from("you cannot reset what has never existed"))
						.send(glob_state)
						.await
				);
				return false;
			}
		};

		let key = _rem.trim();

		if !state.counters.contains_key(key) {
			err_log!(
				msg.respond(format!("i dont know a counter with the name {}", key))
					.send(glob_state)
					.await
			);
			return false;
		}

		if let Some(count) = state.counters.remove(key) {
			err_log!(
				msg.respond(format!("ok, removed {}'s count (it was {})", key, count))
					.send(glob_state)
					.await
			);
		} else {
			err_log!(
				msg.respond(String::from("fuck, i thought that counter exists, but it didnt"))
					.send(glob_state)
					.await
			);
		}

		true
	}

	async fn cmd_nopogey(&mut self, glob_state: &mut State, msg: &Message, _rem: &str) -> bool {
		if !msg.flags.is_privileged() {
			return false;
		}

		let state = self.states.entry(msg.target.clone()).or_default();
		state.no = !state.no;

		if state.no {
			err_log!(
				msg.respond(String::from("ok, no pogeys will be reacted to"))
					.send(glob_state)
					.await
			);
		} else {
			err_log!(
				msg.respond(String::from("ok, starting to react to pogeys")).send(glob_state).await
			);
		}

		true
	}
}

#[async_trait::async_trait]
impl Plugin for RepeaterPlugin {
	async fn init(&mut self, state: &mut State) -> Result<(), String> {
		// let load_state = state.db.load(&self, )
		let load_state = match state.db.load_known::<_, RplugState>(self).await {
			Ok(states) => states,
			Err(error) => return Err(format!("{:?}", error)),
		};

		self.states = load_state;
		for (chan, data) in &mut self.states {
			self.matcher.insert(
				chan.clone(),
				AhoCorasickBuilder::new()
					.match_kind(AhoMatchKind::LeftmostFirst)
					.build(data.reps.iter().map(|el| el.trigger()).collect::<Vec<_>>()),
			);

			for item in &mut data.reps {
				item.reset();
			}
		}

		state.register_command(self, "addpogey");
		state.register_command(self, "addrep");
		state.register_command(self, "delpogey");
		state.register_command(self, "delrep");
		state.register_command(self, "nopogey");
		state.register_command(self, "norep");
		state.register_command(self, "pogeys");
		state.register_command(self, "clearcount");

		Ok(())
	}

	async fn on_message(&mut self, state: &mut State, msg: &Message) -> PlugResult {
		if msg.name == state.current_user().name {
			tracing::trace!("bot user is the same as message user, skipping");
			return PlugResult::Continue;
		}

		let matcher = match self.matcher.get(&msg.target) {
			Some(mtc) => mtc,
			None => {
				tracing::debug!(channel=?msg.target, user=?msg.name, "no matcher yet, skipping");
				return PlugResult::Continue;
			}
		};

		if self.states.get(&msg.target).map(|el| el.no).unwrap_or(false) {
			tracing::debug!("we recieved a pogey, and we found a matcher but we should not pogey!");
			return PlugResult::Stop;
		}

		match matcher.find(&msg.content) {
			Some(found) => {
				let rstate = match self.states.get_mut(&msg.target) {
					Some(state) => state,
					None => {
						tracing::error!(
							"fatal error, unable to find state data for {}, even tho we have a \
							 matcher for it.",
							&msg.target.as_str()
						);
						return PlugResult::Stop;
					}
				};

				let pogs = &mut rstate.reps[found.pattern()];
				let outbound = pogs.build();

				tracing::info!(
					pogey=%pogs.trigger(),
					response=%pogs.response(),
					times=%pogs.current(),
					channel=%msg.target.as_str(),
					user=%msg.name,
					"repeatable triggered"
				);

				if !rstate.counters.contains_key(pogs.trigger()) {
					rstate.counters.insert(pogs.trigger().to_owned(), 1);
				} else {
					let c = rstate
						.counters
						.get_mut(pogs.trigger())
						.expect("sanity error, counter should exist");
					*c += 1;
				}

				err_log!(
					msg.respond(crate::utils::clamp_str(&outbound, 499).to_owned())
						.send(state)
						.await
				);

				PlugResult::Stop
			}
			None => PlugResult::Continue,
		}
	}

	async fn on_command(
		&mut self,
		state: &mut State,
		msg: &Message,
		name: &str,
		remainder: &str,
	) -> PlugResult {
		let r = match name {
			"addpogey" | "addrep" => self.cmd_addpogey(state, msg, remainder).await,
			"delpogey" | "delrep" => self.cmd_delpogey(state, msg, remainder).await,
			"nopogey" | "norep" => self.cmd_nopogey(state, msg, remainder).await,
			"pogeys" => self.cmd_pogeys(state, msg).await,
			"clearcount" => self.cmd_clearcount(state, msg, remainder).await,
			_ => {
				tracing::warn!(
					"we registered for the command {}, but it was never added to dispatch!",
					name
				);
				return PlugResult::Continue;
			}
		};

		if r {
			if let Some(data) = self.states.get(&msg.target) {
				if let Err(e) = state.db.store(&*self, &msg.target, &data).await {
					tracing::warn!(error=?e, "failed to store pogey state!");
				}
			}
		}

		PlugResult::Stop
	}

	async fn stop(&mut self, state: Option<&mut State>) {
		if let Some(state) = state {
			for (chn, data) in &self.states {
				if let Err(why) = state.db.store::<_, RplugState>(self, chn, data).await {
					tracing::warn!(
						"failed to store repeater state for {}! {:?}",
						chn.as_str(),
						why
					);
				}
			}
		}
	}

	fn priority(&self) -> usize {
		100
	}
	fn name(&self) -> &'static str {
		"repeatable"
	}
}
