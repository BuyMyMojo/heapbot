use super::prelude::*;
use crate::model::twitch_api::{TwitchApi, TwitchApiMarker};
use chrono::{DateTime, Utc};
use rand::Rng;
use serde::{Deserialize, Serialize};
use std::{
	collections::{BTreeMap, HashMap},
	sync::Arc,
};
use tokio::sync::Mutex;

const QID_LEN: usize = 4;
static QID_CHR: [char; 36] = [
	'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
	't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
];

#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
struct Quote {
	pub text: String,
	pub creator: String,
	pub game: Option<String>,
	pub time: DateTime<Utc>,
}

#[derive(Debug, Default, Serialize, Deserialize)]
struct QuoteState {
	quotes: BTreeMap<String, Quote>,
}

#[derive(Debug, Default)]
pub struct QuotePlugin {
	states: HashMap<Channel, QuoteState>,
}

impl QuotePlugin {
	async fn cmd_quote(&mut self, glob_state: &mut State, msg: &Message, rem: &str) -> bool {
		let state = match self.states.get(&msg.target) {
			Some(sta) => sta,
			None => {
				tracing::debug!("no quotes yet");
				return false;
			}
		};

		if state.quotes.is_empty() {
			return false;
		}

		match Self::grab_qid(rem.trim()).map(|el| el.to_ascii_lowercase()) {
			None => {
				let id_index = rand::thread_rng().gen_range(0, state.quotes.len());
				// These two unwrap *should* be sane, as we are generating a valid id index (we
				// generate it off the lengt) and then directly after that retrieving that exact
				// id from the map.
				let id = state.quotes.keys().nth(id_index).unwrap();
				let qt = state.quotes.get(id).unwrap();
				err_log!(msg.respond(Self::format_quote(id, qt)).send(glob_state).await);
			}
			Some(id) => match state.quotes.get(&id) {
				Some(qt) => {
					err_log!(msg.respond(Self::format_quote(&id, qt)).send(glob_state).await);
				}
				None => {
					err_log!(
						msg.respond(String::from(
							"to quote Hamlet Act III, Scene iii Line 87: \"No\"",
						))
						.send(glob_state)
						.await
					);
				}
			},
		}

		false
	}

	async fn cmd_blamequote(&mut self, glob_state: &mut State, msg: &Message, rem: &str) -> bool {
		let state = match self.states.get_mut(&msg.target) {
			Some(sta) => sta,
			None => {
				tracing::debug!("no quotes to delete");
				err_log!(msg.respond(String::from("i cant find that")).send(glob_state).await);
				return false;
			}
		};

		if state.quotes.is_empty() {
			return false;
		}

		let qid = match Self::grab_qid(rem.trim()).map(|el| el.to_ascii_lowercase()) {
			Some(id) => id,
			None => {
				err_log!(
					msg.respond(String::from(
						"that id is to blame on [you need the \"discovery dlc\" to see this, buy \
						 now for only 9.99$!]"
					))
					.send(glob_state)
					.await
				);
				return false;
			}
		};

		match state.quotes.get(&qid) {
			Some(quote) => {
				tracing::info!(qid=%qid, "blaming quote");
				err_log!(
					msg.respond(format!("you can blame the quote #{} on {}", qid, quote.creator))
						.send(glob_state)
						.await
				);
			}
			None => {
				err_log!(
					msg.respond(String::from(
						"i cant pull stuff out of my arse you know? (invalid id)"
					))
					.send(glob_state)
					.await
				);
			}
		}

		false
	}

	async fn cmd_addquote(&mut self, glob_state: &mut State, msg: &Message, rem: &str) -> bool {
		// if user is not privleged or BINGUS
		if !msg.flags.is_privileged() || !msg.flags.is_mojo() {
			return false;
		}

		let text = rem.trim();
		if text.is_empty() {
			err_log!(
				msg.respond(String::from(
					"what do you want me to do, sing a song? (i cant add empty quotes)",
				))
				.send(glob_state)
				.await
			);
			return false;
		}

		let state = self.states.entry(msg.target.clone()).or_default();

		let mut nid = Self::gen_id();
		let mut tries: usize = 0;
		while state.quotes.contains_key(&nid) {
			if tries > 300 {
				tracing::warn!(channel=%msg.target.as_str(), tries=300, "id starvation");
				let _ = msg
					.respond(String::from(
						"ran out of ids to generate, please tell heap about this!",
					))
					.send(glob_state)
					.await;
			}
			nid = Self::gen_id();
			tries += 1;
		}

		let mut game = None;

		let http = glob_state.http();
		let twapi = Arc::clone(
			&glob_state
				.data
				.get::<TwitchApiMarker>()
				.expect("fatal failure, init never inserted api marker"),
		);
		{
			let mut twapi_lock = twapi.lock().await;
			match twapi_lock.get_stream_info(&http, &[], &[msg.target.as_str_clean()]).await {
				Ok(stream_infos) => match stream_infos.data.get(0) {
					Some(stream_info) => {
						match twapi_lock.get_game_info(glob_state, &stream_info.game_id).await {
							Ok(Some(gn)) => game = Some(gn),
							Ok(None) => tracing::trace!("no game was found"),
							Err(why) => tracing::trace!(error=?why, "failed to retrieve game info"),
						}
					}
					None => tracing::trace!("no streams seen"),
				},
				Err(why) => tracing::trace!(error=?why, "failed to retrieve stream info"),
			}
		}

		tracing::info!(id=%nid, creator=%msg.name, game_found=?game, "added quote");

		// TODO: Add game fetching
		state.quotes.insert(
			nid.clone(),
			Quote {
				text: text.to_owned(),
				creator: msg.name.clone(),
				game: game.clone(),
				time: Utc::now(),
			},
		);

		err_log!(
			msg.respond(format!(
				"added quote #{}!{}",
				nid,
				if let Some(gm) = game {
					format!(" (Game = {})", gm)
				} else {
					format!(
						" (i was unable to find the game you are playing, please use !!quote_game \
						 #{} <game here> to set it)",
						nid
					)
				},
			))
			.send(glob_state)
			.await
		);

		true
	}

	async fn cmd_delquote(&mut self, glob_state: &mut State, msg: &Message, rem: &str) -> bool {
		if !msg.flags.is_privileged() {
			return false;
		}

		let state = match self.states.get_mut(&msg.target) {
			Some(sta) => sta,
			None => {
				tracing::debug!("no quotes to delete");
				err_log!(msg.respond(String::from("i cant find that")).send(glob_state).await);
				return false;
			}
		};

		let id = match Self::grab_qid(rem.trim()).map(|el| el.to_ascii_lowercase()) {
			Some(id) => id,
			None => {
				return false;
			}
		};

		if state.quotes.remove(&id).is_some() {
			tracing::info!(qid=%id, "removed quote");
			err_log!(msg.respond(format!("removed quote #{}", id)).send(glob_state).await);
			true
		} else {
			err_log!(msg.respond(format!("unable to find quote #{}", id)).send(glob_state).await);
			false
		}
	}

	async fn cmd_quote_game(&mut self, glob_state: &mut State, msg: &Message, rem: &str) -> bool {
		if !msg.flags.is_privileged() {
			return false;
		}

		let state = match self.states.get_mut(&msg.target) {
			Some(sta) => sta,
			None => {
				tracing::debug!("no quotes to delete");
				err_log!(msg.respond(String::from("i cant find that")).send(glob_state).await);
				return false;
			}
		};

		let parts = rem.trim().splitn(2, ' ').collect::<Vec<_>>();
		let new_game = if parts.len() == 2 { Some(&parts[1]) } else { None };

		let qid = match Self::grab_qid(&parts[0])
			.and_then(|el| if el.is_empty() { None } else { Some(el) })
			.map(|el| el.to_ascii_lowercase())
		{
			None => {
				err_log!(
					msg.respond(String::from("i cannot do this without a ID deccHuh"))
						.send(glob_state)
						.await
				);
				return false;
			}
			Some(id) => id,
		};

		if !state.quotes.contains_key(&qid) {
			err_log!(
				msg.respond(String::from("i dont know a quote with that id deccShrug"))
					.send(glob_state)
					.await
			);
			return false;
		}

		match new_game {
			Some(gam) => {
				tracing::info!(qid=%qid, game=%gam, "set game for quote");
				err_log!(
					msg.respond(format!("Set the game for quote #{} to {}", qid, gam))
						.send(glob_state)
						.await
				);
			}
			None => {
				tracing::info!(qid=%qid, "cleared game for quote");
				err_log!(
					msg.respond(format!("Cleared the game for quote #{}", qid))
						.send(glob_state)
						.await
				);
			}
		}

		state.quotes.get_mut(&qid).unwrap().game = new_game.map(|el| String::from(*el));
		true
	}

	/// Grab what *might* be a id
	fn grab_qid(s: &str) -> Option<&str> {
		if s.is_empty() {
			return None;
		}
		if s.starts_with('#') {
			Some(&s[1..])
		} else {
			Some(&s)
		}
	}

	// TODO: Move this to a Display implementation
	/// Format a quote to a string
	#[inline]
	fn format_quote(id: &str, qt: &Quote) -> String {
		format!(
			"{text} [{date}]{game} [#{id}]",
			text = qt.text,
			date = qt.time.format("%F"),
			id = id,
			game = match &qt.game {
				Some(gam) => format!(" [{}]", gam),
				None => String::new(),
			}
		)
	}

	/// Generate a random QID_LEN long id, comprised of QID_CHR chars
	fn gen_id() -> String {
		let mut rng = rand::thread_rng();
		let mut out = String::with_capacity(QID_LEN);

		for _ in 0..QID_LEN {
			out.push(QID_CHR[rng.gen_range(0, QID_CHR.len())]);
		}

		out
	}
}

#[async_trait::async_trait]
impl Plugin for QuotePlugin {
	async fn init(&mut self, state: &mut State) -> Result<(), String> {
		let load_state = match state.db.load_known::<_, QuoteState>(self).await {
			Ok(states) => states,
			Err(error) => return Err(format!("{:?}", error)),
		};

		self.states = load_state;

		state.register_command(self, "quote");
		state.register_command(self, "addquote");
		state.register_command(self, "delquote");
		state.register_command(self, "quote_game");
		state.register_command(self, "blamequote");

		// If it hasent been done yet, register a twitch api client
		if !state.data.contains::<TwitchApiMarker>() {
			let creds = state.get_api_creds().clone();
			state.data.insert::<TwitchApiMarker>(Arc::new(Mutex::new(TwitchApi::new(
				&creds.client_id,
				&creds.client_secret,
				30,
				60 * 1000,
			))));
		}

		Ok(())
	}

	async fn on_command(
		&mut self,
		state: &mut State,
		msg: &Message,
		name: &str,
		remainder: &str,
	) -> PlugResult {
		let r = match name {
			"quote" => self.cmd_quote(state, msg, remainder).await,
			"addquote" => self.cmd_addquote(state, msg, remainder).await,
			"delquote" => self.cmd_delquote(state, msg, remainder).await,
			"quote_game" => self.cmd_quote_game(state, msg, remainder).await,
			"blamequote" => self.cmd_blamequote(state, msg, remainder).await,
			_ => {
				tracing::warn!(
					"we registered for the command {}, but it was never added to dispatch!",
					name
				);
				return PlugResult::Continue;
			}
		};

		if r {
			if let Some(data) = self.states.get(&msg.target) {
				if let Err(e) = state.db.store(&*self, &msg.target, &data).await {
					tracing::warn!(error=?e, "failed to store quote state!");
				}
			}
		}

		PlugResult::Stop
	}

	async fn stop(&mut self, state: Option<&mut State>) {
		if let Some(state) = state {
			for (chn, data) in &self.states {
				if let Err(why) = state.db.store::<_, QuoteState>(self, chn, data).await {
					tracing::warn!("failed to store quote state for {}! {:?}", chn.as_str(), why);
				}
			}
		}
	}

	fn priority(&self) -> usize {
		25
	}
	fn name(&self) -> &'static str {
		"quotes"
	}
}
