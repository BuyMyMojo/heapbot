use crate::{
	db::DB,
	model::{
		bot::{CurrentUser, PlugResult},
		Channel, Flags, Message, Response, RlOpts, RlSender, State, TIRCConnectionInfo,
	},
};
use irc::{
	client::prelude::{Client, Command, Config as IrcConfig, IrcReactor},
	proto::command::{CapSubCommand, Command as IrcCommand},
};
use std::{
	collections::HashMap,
	ops::{Deref, DerefMut},
	sync::{
		atomic::{AtomicBool, Ordering as AtomicOrdering},
		Arc,
	},
	thread,
	thread::JoinHandle,
};
use tokio::{
	fs::File,
	io::AsyncWriteExt,
	runtime::Handle,
	sync::mpsc::{channel as mpsc_channel, Receiver, Sender},
};
use tracing_futures::Instrument;

pub mod plugins;

pub type Html = maud::PreEscaped<String>;

const PREFIX: &str = "!!";

/// All plugins for the bot *must* use this trait.
/// The trait is requiring the implementor to be send, so it can be safely sent
/// between threads on the async executor
#[async_trait::async_trait]
pub trait Plugin: Send {
	/// Called once at bot start
	async fn init(&mut self, _: &mut State) -> Result<(), String> {
		Ok(())
	}
	/// Called on every recieved message that has not been prevented from
	/// bubbling up the chain and has not been identified as a known command
	async fn on_message(&mut self, _: &mut State, _: &Message) -> PlugResult {
		PlugResult::Continue
	}
	/// This is called whenever a registered command was found for the plugin.
	async fn on_command(&mut self, _: &mut State, _: &Message, _: &str, _: &str) -> PlugResult {
		PlugResult::Stop
	}
	/// Called on a gracefull bot stop
	async fn stop(&mut self, _: Option<&mut State>) {}
	/// Generate a fragment that visualizes the state of the plugin
	async fn state_html(&mut self, _: &mut State) -> Option<Html> {
		None
	}

	/// Priority. Used for execution ordering. 0 = Highest
	fn priority(&self) -> usize;
	/// The name of the plugin (should only contain the characters
	/// `[a-z-A-Z0-9_-]`)
	fn name(&self) -> &'static str;
}

struct RegisteredPlugin {
	pub name: String,
	pub priority: usize,
	pub plug: Box<dyn Plugin>,
}

impl Deref for RegisteredPlugin {
	type Target = Box<dyn Plugin>;
	fn deref(&self) -> &Self::Target {
		&self.plug
	}
}

impl DerefMut for RegisteredPlugin {
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.plug
	}
}

impl From<Box<dyn Plugin>> for RegisteredPlugin {
	fn from(o: Box<dyn Plugin>) -> RegisteredPlugin {
		RegisteredPlugin { name: o.name().to_owned(), priority: o.priority(), plug: o }
	}
}

/// A twitch bot, with registered plugins
pub struct TwitchBot {
	/// All the registered plugins
	plugs: Vec<RegisteredPlugin>,
	/// Current state (including commands)
	state: State,
	/// The marker, signaling to the worker thread when/if to stop
	worker_marker: Arc<AtomicBool>,
	// This will be needed for
	/// Handle to the worker thread (to wait for exit)
	#[allow(dead_code)]
	worker_handle: JoinHandle<()>,
	/// Inbound message channel
	inbound_mesg: Receiver<Event>,
}

impl TwitchBot {
	/// Create a new twitchbot
	pub fn new(
		db: DB,
		runtime_handle: Handle,
		conni: TIRCConnectionInfo,
		startch: Vec<Channel>,
		tw_creds: crate::config::App,
	) -> TwitchBot {
		let wrk_bool = Arc::new(AtomicBool::new(false));
		let (mesg_tx, mesg_rx) = mpsc_channel(32);
		// As outbound will be ratelimited, we can keep the channel small
		let (resp_tx, resp_rx) = mpsc_channel(8);
		TwitchBot {
			plugs: vec![RegisteredPlugin::from(Box::new(GOpts) as Box<dyn Plugin>)],
			state: State::new(
				db,
				RlSender::new(resp_tx, RlOpts::new(20, 30 * 1000)),
				CurrentUser::new(&conni.user),
				tw_creds,
			),
			worker_marker: Arc::clone(&wrk_bool),
			worker_handle: Self::worker(wrk_bool, runtime_handle, resp_rx, mesg_tx, conni, startch),
			inbound_mesg: mesg_rx,
		}
	}

	/// Register a plugin with the bot.
	pub fn register_plugin(&mut self, plug: Box<dyn Plugin>) -> bool {
		if self.plugs.iter().any(|e| e.name() == plug.name()) {
			return false;
		}
		tracing::debug!(plug=%plug.name(), priority=%plug.priority(), "registering plugin");
		self.plugs.push(plug.into());
		self.plugs.sort_by_key(|k| k.priority());
		true
	}

	/// Run the bot, this should be spawned onto the executor
	pub async fn run(mut self) {
		let span = tracing::info_span!("run");
		let _guard = span.enter();

		tracing::info!("signaling worker to start up");
		self.worker_marker.store(true, AtomicOrdering::Relaxed);

		for plug in &mut self.plugs {
			if let Err(why) = plug.init(&mut self.state).await {
				tracing::error!("plugin {} failed initialization: {:?}", plug.name(), why);
				return;
			}
		}

		'primary_loop: loop {
			let msg = match self.inbound_mesg.recv().await {
				Some(Event::Message { data: v }) => v,
				Some(Event::Ping) => {
					self.ping().await;
					continue;
				}
				None => {
					for plug in &mut self.plugs {
						plug.stop(Some(&mut self.state)).await;
					}
					self.worker_marker.store(true, AtomicOrdering::SeqCst);
					// TODO: Actually wait for the worker to gracefully exit
					break;
				}
			};

			if msg.name == self.state.current_user().name {
				self.state.mark_send(&msg.target);
			}

			if msg.content.len() > PREFIX.len() && msg.content.starts_with(PREFIX) {
				tracing::debug!("message is a command");

				let name_end_idx = msg
					.content
					.chars()
					.enumerate()
					.find(|(_, bt)| *bt == ' ')
					.map(|(idx, _)| idx)
					.unwrap_or_else(|| msg.content.len());

				// 2020-05-02: For some reason we need this for rustc to stop complaining about
				// the lifetime of self.state. I am unshure where this comes from, but this
				// works (llvm should inline this anyway because its only called here, but we
				// are going for safe here and mark it as inline(always) anyway)
				#[inline(always)]
				fn async_is_a_bitch_sometimes_man_fuck_lifetimes(
					s: &State,
					content: &str,
					name_end_idx: usize,
				) -> Option<crate::model::bot::Command> {
					s.get_command(&content[PREFIX.len()..name_end_idx]).cloned()
				}
				let mcom = async_is_a_bitch_sometimes_man_fuck_lifetimes(
					&self.state,
					&msg.content,
					name_end_idx,
				);

				if let Some(command) = mcom {
					for plug in &mut self.plugs {
						if plug.name() == command.ns() {
							let eidx = if name_end_idx == msg.content.len() {
								name_end_idx
							} else {
								name_end_idx + 1
							};
							let plug_name = plug.name().to_owned();
							match plug
								.on_command(
									&mut self.state,
									&msg,
									&msg.content[PREFIX.len()..name_end_idx],
									&msg.content[eidx..],
								)
								.instrument(tracing::info_span!(
									"on_command",
									plugin=%plug_name,
									cn=%&msg.content[PREFIX.len()..name_end_idx]
								))
								.await
							{
								PlugResult::Continue => (),
								PlugResult::Stop => continue 'primary_loop,
								PlugResult::Error { message, .. } => {
									tracing::warn!(message=?message, "command errored");
									continue 'primary_loop;
								}
							}
						}
					}
				}

				tracing::debug!(
					"we got something that looked like a command, but wasnt. continuing"
				);
			}



			for plug in &mut self.plugs {
				let plug_name = plug.name().to_owned();
				match plug
					.on_message(&mut self.state, &msg)
					.instrument(tracing::info_span!("on_message", plugin=%plug_name))
					.await
				{
					PlugResult::Continue => (),
					PlugResult::Stop => break,
					PlugResult::Error { message: error, report } => {
						tracing::warn!(report=%report, message=?error, "plugin errored during execution");
						if report {
							if let Err(why) = msg
								.respond(format!("COMFAIL: {}", error))
								.send(&mut self.state)
								.await
							{
								tracing::error!(reason=?why, "send failure");
							}
						}
					}
				}
			}
		}
	}

	/// Executed on every ping from twitch (usually ~5min)
	async fn ping(&mut self) {
		#[cfg(debug_assertions)]
		static BULMA_CSS: &[u8] = include_bytes!("../../vendor/bulma/css/bulma.css");
		#[cfg(not(debug_assertions))]
		static BULMA_CSS: &[u8] = include_bytes!("../../vendor/bulma/css/bulma.min.css");

		struct HtmlState {
			plugins: Vec<(u16, String, Html)>,
		}
		let mut states = HashMap::<Channel, HtmlState>::new();

		let outdir = self.state.db.get_special("html").await.unwrap();
		for (channel, state) in states {
			let html = maud::html! {
				(maud::DOCTYPE);
				html {
					head {
						meta charset="utf-8";
						meta description=(format!("Stats for {}", channel.as_str_clean()));
						meta name="viewport" content="width=device-width, initial-scale=1.0";
						link rel="stylesheet" href="./common.css";
						link rel="stylesheet" href=(format!("./{}.css", channel.as_str_clean()));
						title { "Stats for {}" (channel.as_str_clean()) }
					}
					body {

					}
				}
			};

			let p = outdir.join(channel.as_str_clean());
			let mut f = File::create(&p).await.unwrap();
			f.write_all(html.into_string().as_bytes()).await.unwrap();
		}
	}

	/// Worker fn, handles the irc connection part
	fn worker(
		state_handle: Arc<AtomicBool>,
		runtime_handle: Handle,
		rx: Receiver<Response>,
		tx: Sender<Event>,
		irc_conn_info: TIRCConnectionInfo,
		initial_channels: Vec<Channel>,
	) -> JoinHandle<()> {
		thread::Builder::new()
			.name(String::from("irc-gateway"))
			.spawn(move || {
				let irc_span = tracing::info_span!("irc");
				let _guard_irc_span = irc_span.enter();
				// Wait to do anything until the bot signals us that we can start
				// We do not particulary care about ordering here, as we "only" loose up to
				// ~100ms on start
				while !state_handle.load(AtomicOrdering::Relaxed) {
					thread::sleep(std::time::Duration::from_millis(100));
				}

				let setup_span = tracing::info_span!("setup_connect");
				let _guard_setup_span = setup_span.enter();

				let connection_config = IrcConfig {
					server: Some(irc_conn_info.endpoint.clone()),
					port: Some(irc_conn_info.port),
					use_ssl: Some(true),
					..Default::default()
				};

				let mut irc_react = match IrcReactor::new() {
					Ok(rc) => rc,
					Err(e) => {
						tracing::error!("fatal error creating reactor: {:?}", e);
						return;
					}
				};

				let client = match irc_react.prepare_client_and_connect(&connection_config) {
					Ok(cli) => cli,
					Err(e) => {
						tracing::error!("fatal error creating and connecting client: {:?}", e);
						return;
					}
				};

				tracing::debug_span!("auth").in_scope(|| {
					tracing::debug!(target = "oauth");
					client
						.send(IrcCommand::Raw("PASS".to_owned(), vec![irc_conn_info.oauth], None))
						.expect("failed to send pass");
					tracing::debug!(target = "nick");
					client
						.send(IrcCommand::Raw("NICK".to_owned(), vec![irc_conn_info.user], None))
						.expect("failed to send nick");
				});

				tracing::debug_span!("capabilities").in_scope(|| {
					tracing::debug!(cap = "membership");
					client
						.send(Command::CAP(
							None,
							CapSubCommand::REQ,
							Some(":twitch.tv/membership".to_owned()),
							None,
						))
						.expect("Unable to set Membership Capability");

					tracing::debug!(cap = "tags");
					client
						.send(Command::CAP(
							None,
							CapSubCommand::REQ,
							Some(":twitch.tv/tags".to_owned()),
							None,
						))
						.expect("Unable to set Channel Tag Capabilies");

					tracing::debug!(cap = "command");
					client
						.send(Command::CAP(
							None,
							CapSubCommand::REQ,
							Some(":twitch.tv/commands".to_owned()),
							None,
						))
						.expect("Unable to set Channel Tag Capabilies");
				});

				tracing::debug_span!("init_chann").in_scope(|| {
					for chn in initial_channels {
						tracing::debug!(chan = %chn.as_str());
						client
							.send(Command::JOIN(chn.as_str().to_owned(), None, None))
							.expect("failed to join channel");
						thread::sleep(std::time::Duration::from_secs(1));
					}
				});

				tracing::debug!("registering message handler");

				// this is the message handler, handling inbound connections to the bot
				let client_copy = client.clone();
				let rt = runtime_handle.clone();
				irc_react.register_client_with_handler(client, move |_, message| {
					match message.command {
						IrcCommand::NOTICE(c, n) => {
							tracing::trace!(channel=%c, notice=%n, "ttf notice");
							Ok(())
						}
						IrcCommand::PRIVMSG(_, _) => {
							if let Some(parsed) = Message::parse_irc(message) {
								let mut txc = tx.clone();
								rt.enter(|| {
									tokio::spawn(async move {
										txc.send(Event::Message { data: parsed }).await
									})
								});
							}
							Ok(())
						}
						_ => {
							tracing::trace!(message=?message, "unknown message");
							Ok(())
						}
					}
				});

				// this is the outbound end for the bot. It is a "dumb" sender, and does not
				// validate any ratelimits.
				// TODO: Reconsider moving the ratelimit to this
				let mut rx = rx;
				runtime_handle.enter(|| {
					tokio::spawn(async move {
						while let Some(outbound) = rx.recv().await {
							match client_copy.send(IrcCommand::PRIVMSG(
								outbound.target.as_str().to_owned(),
								outbound.message,
							)) {
								Ok(_) => (),
								Err(e) => {
									tracing::error!("failed to end message: {:?}", e);
									break;
								}
							}
						}
					});
				});

				drop(_guard_setup_span);

				if let Err(why) = irc_react.run() {
					tracing::error!(result=?why, "reactor failed");
				}
			})
			.expect("unable to start worker thread")
	}
}

#[derive(Debug, Clone)]
enum Event {
	Ping,
	Message { data: Message },
}

struct GOpts;

#[async_trait::async_trait]
impl Plugin for GOpts {
	async fn init(&mut self, state: &mut State) -> Result<(), String> {
		macro_rules! reg_or_fail {
			($i:expr, $s:expr, $n:expr) => {
				if !$i.register_command($s, $n) {
					tracing::error!(concat!(
						"failed to register ",
						$n,
						"! is this plugin the first?"
					));
					return Err(String::from(concat!(
						"failed to register ",
						$n,
						"! is this plugin the first?"
					)));
					}
			};
		}

		reg_or_fail!(state, self, "abuse");
		reg_or_fail!(state, self, "no_limit");
		reg_or_fail!(state, self, "nolimit");
		reg_or_fail!(state, self, "no_limits");

		Ok(())
	}

	async fn on_message(&mut self, s: &mut State, m: &Message) -> PlugResult {
		if let Some(c) = s.get_channel_cfg(&m.target).await {
			if c.flags.contains(Flags::ABUSE) {
				return PlugResult::Stop
			}
		}
		PlugResult::Continue
	}

	async fn on_command(&mut self, s: &mut State, m: &Message, n: &str, _: &str) -> PlugResult {
		let mut dirty = false;
		match n {
			"abuse" => {
				if m.flags.is_high_privileged() {
					dirty = true;
					if let Some(c) = s.get_channel_cfg(&m.target).await {
						c.flags.toggle(Flags::ABUSE);

						if c.flags.contains(Flags::ABUSE) {
							tracing::info!("enabled abuse mode");
							err_log!(
								m.respond(String::from(
									"Abuse mode is now enabled. refusing to process further \
									 message events.",
								))
								.send(s)
								.await
							);
						} else {
							tracing::info!("disabled abuse mode");
							err_log!(
								m.respond(String::from(
									"Abuse mode is now disabled. resumed processing message \
									 events.",
								))
								.send(s)
								.await
							);
						}
					}
				}
			}
			"no_limit" | "nolimit" | "no_limits" => {
				if m.flags.is_high_privileged() {
					dirty = true;
					if let Some(c) = s.get_channel_cfg(&m.target).await {
						c.flags.toggle(Flags::NO_LIMIT);

						if c.flags.contains(Flags::NO_LIMIT) {
							tracing::info!("enabled no_limit mode");
							err_log!(
								m.respond(String::from("No Limit mode is now enabled!"))
									.send(s)
									.await
							);
						} else {
							tracing::info!("disabled no_limit mode");
						}
					}
				}
			}
			_ => return PlugResult::Continue,
		}

		if dirty {
			s.store_channel_cfg(&m.target).await
		};

		PlugResult::Stop
	}

	fn priority(&self) -> usize {
		0
	}
	fn name(&self) -> &'static str {
		"glob_opts"
	}
}
