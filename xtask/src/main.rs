use std::error::Error;
use std::path::{Path, PathBuf};
use std::process::Command;
use std::{env, fs, process};

type DynError = Box<dyn Error>;

const E_ESCAPE: &str = "\x1B[1m\x1B[31mERROR\x1B[0m";

fn main() {
	if let Err(why) = realmain() {
		eprintln!("{}", why);
		process::exit(-1);
	}
}

fn realmain() -> Result<(), DynError> {
	let task = env::args().nth(1);
	let adoc = env::args().nth(2);

	let adoc_path = adoc.unwrap_or_else(|| "asciidoctor".to_owned());
	match task.as_ref().map(|it| it.as_str()) {
		Some("doc") => doc(&adoc_path)?,
		_ => print_help(),
	}
	Ok(())
}

fn print_help() {
	eprintln!(
		"Tasks:

doc [adoc path]           Generate documentation
"
	);
}

fn doc(adoc_path: &str) -> Result<(), DynError> {
	check_command(adoc_path)?;
	check_dir(&doc_dir())?;

	eprintln!("generating heapbot documentation...");
	let mut child = Command::new(adoc_path)
		.args(&[
			"-b",
			"html5",
			"-o",
			doc_dir().join("heapbot-doc.html").to_string_lossy().as_ref(),
			"-S",
			"server",
			"-n",
			project_root().join("comdoc/heapbot.adoc").to_string_lossy().as_ref(),
		])
		.spawn()
		.unwrap();

	if !child.wait().unwrap().success() {
		return Err("failed to generate doc".into());
	} else {
		eprintln!("done");
	}

	Ok(())
}

/// Try to execute the command without arguments and then immediately kill it again
fn check_command(command: &str) -> Result<(), DynError> {
	Command::new(command)
		.spawn()
		.and_then(|mut it| it.kill())
		.map_err(|err| format!("{} missing command {}: {:?}", E_ESCAPE, command, err).into())
}

fn project_root() -> PathBuf {
	Path::new(&env!("CARGO_MANIFEST_DIR")).ancestors().nth(1).unwrap().to_path_buf()
}

fn doc_dir() -> PathBuf {
	project_root().join("target/command_documentation")
}

fn check_dir(dir: impl AsRef<Path>) -> Result<(), DynError> {
	let path = dir.as_ref();
	if !path.exists() {
		fs::create_dir_all(path)?;
	}
	Ok(())
}
